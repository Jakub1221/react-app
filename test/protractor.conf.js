exports.config = {
    baseUrl: 'http://localhost:9000',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['./specs/simple.js'],
    onPrepare: function() {
        browser.ignoreSynchronization = true;
    }
}