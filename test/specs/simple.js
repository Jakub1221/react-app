// simple test
describe('simple test', function() {
    it('new note redirection', function() {
        browser.get('/');
        browser.findElement(by.className('btn-addnewnote')).click();
        browser.sleep(2000).then(function() {
            expect(browser.getCurrentUrl()).toContain("/new");
        });
    });
});