import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Translate } from 'react-localize-redux';

import { loadNoteDetail } from '../../actions/notes.actions';

import './index.scss';

class NoteDetail extends Component {

  render() {

    return (
      <div className={`row`}>
        <div className={`col-12 text-left mb-3`}>
            {this.props.noteDetail}
        </div>
        <div className={`col-12 text-right`}>
            <Link to="/">
                <button className={`btn btn-success`} type="button">
                    <Translate id="basic.return" />
                </button>
            </Link>
        </div>
      </div>
    )
  }

    componentDidMount() {
        const { noteid } = this.props.match.params;
        this.props.loadNoteDetail(noteid);
    }
}

NoteDetail.propTypes = {
    noteDetail: PropTypes.string,
    loadNoteDetail: PropTypes.func,
    match: PropTypes.object,
};

const mapStateToProps = (state) => ({
    noteDetail: state.noteDetail
});

const mapDispatchToProps = (dispatch) => ({ 
    loadNoteDetail: (id) => {
        loadNoteDetail(id)(dispatch);
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(NoteDetail);