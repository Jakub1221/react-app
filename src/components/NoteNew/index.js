import React, { Component } from 'react'
import { newNote } from '../../actions/notes.actions';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom'
import { Translate } from 'react-localize-redux';
import PropTypes from 'prop-types';

import './index.scss'

class NoteNew extends Component {
    constructor(props) {
        super(props);

        this.state = {
            newTitle: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ newTitle: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.newNote(this.newTitle);
        this.props.history.push("/");
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className={`row`}>
                    <div className={`col-12`}>
                        <Translate id="basic.title" />:
                        <br />
                        <textarea className={`textarea-input`} type="text" value={this.state.newTitle} onChange={this.handleChange} />
                    </div>
                </div>
                <div className={`row mt-3`}>
                    <div className={`col-12 text-right`}>
                        <Link to="/">
                            <button className={`btn btn-warning`} type="button">
                                <Translate id="basic.cancel" />
                            </button>
                        </Link>
                        <button className={`btn btn-success ml-3`} type="submit">
                            <Translate id="basic.create" />
                        </button>
                    </div>
                </div>
            </form>
        )
    }
}

NoteNew.propTypes = {
    history: PropTypes.object,
    newNote: PropTypes.func,
};

const mapDispatchToProps = (dispatch) => ({ 
    newNote: (newTitle) => {
        newNote(newTitle)(dispatch);
    }
});

export default connect(null, mapDispatchToProps)(withRouter(NoteNew));