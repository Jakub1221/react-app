import React from "react";
import { withLocalize } from "react-localize-redux";
import PropTypes from 'prop-types';

const LanguageToggle = ({ languages, activeLanguage, setActiveLanguage }) => (
  <div>
    {languages.map(lang => (
    <button 
        className={`btn btn-primary ml-3`}
        disabled={lang.code === activeLanguage.code}
        key={lang.code}
        onClick={() => setActiveLanguage(lang.code)}
        >
        {lang.name}
    </button>
    ))}
  </div>
);

LanguageToggle.propTypes = {
  languages: PropTypes.array,
  activeLanguage: PropTypes.object,
  setActiveLanguage: PropTypes.func,
};

export default withLocalize(LanguageToggle);