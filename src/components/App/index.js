import React, { Component } from 'react'
import NoteList from '../NoteList';
import { Route } from 'react-router-dom';
import { renderToStaticMarkup } from "react-dom/server";
import { withLocalize, Translate } from "react-localize-redux";
import PropTypes from 'prop-types';

import NoteDetail from '../NoteDetail';
import NoteEdit from '../NoteEdit';
import NoteNew from '../NoteNew';
import defaultTranslation from "../../translation.default.json";
import LanguageToggle from "./lang_toggle";

import './index.scss';

class App extends Component {
    constructor(props) {
        super(props);

        props.initialize({
            languages: [
            { name: "English", code: "en" },
                { name: "Slovak", code: "sk" }
            ],
            translation: defaultTranslation,
            options: { renderToStaticMarkup },
        });
    }

    render() {
        return (
            <div className={`container-fluid`}>
                <div className={`row mt-5`}>
                    <div className={`offset-1 col-5 text-left`}>
                    <Translate id="basic.header_msg" />
                    </div>
                    <div className={`col-5 text-right`}>
                    <LanguageToggle />
                    </div>
                </div>
                <div className={`row my-5`}>
                    <div className={`offset-1 col-10 bottom-line`}>
                    </div>
                </div>
                <div className={`row`}>
                    <div className={`offset-1 col-10`}>
                        <Route exact path="/" component={NoteList}/>
                        <Route path="/new" component={NoteNew}/>
                        <Route path="/detail/:noteid" component={NoteDetail}/>
                        <Route path="/edit/:noteid" component={NoteEdit}/>
                    </div>
                </div>
            </div>
        )
    }
}

App.propTypes = {
    initialize: PropTypes.func,
};

export default withLocalize(App);
