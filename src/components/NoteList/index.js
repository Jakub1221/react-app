import React, { Component } from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Translate } from "react-localize-redux";
import { loadNotes } from '../../actions/notes.actions';
import NoteListItem from './item';

import './index.scss';

class NoteList extends Component {
    render() {
        const { notes } = this.props;

        const notesRender = notes.map((note) => <NoteListItem key={note.id} id={note.id} title={note.title} />);
        notesRender.push(
            <tr key="x">
                <td></td>
                <td></td>
                <td className={`text-right`}>
                    <Link to="/new">
                        <button className={`btn btn-success btn-addnewnote`}><Translate id="basic.addnewnote" /></button>
                    </Link>
                </td>
            </tr>
        );

        return ( 
        <table className={`table`}>
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col"> <Translate id="basic.title" /></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
            {
                notesRender
            }
            </tbody>
        </table>
        )
    }

    componentDidMount() {
        if (this.props.firstLoad) {
            this.props.loadNotes();
        }
    }
}


NoteList.propTypes = {
    notes: PropTypes.array,
    firstLoad: PropTypes.bool,
    loadNotes: PropTypes.func,
};

const mapStateToProps = (state) => ({ 
    notes: state.notes,
    firstLoad: state.firstLoad,
});
  
const mapDispatchToProps = (dispatch) => ({ 
    loadNotes: () => {
        loadNotes()(dispatch);
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(NoteList);