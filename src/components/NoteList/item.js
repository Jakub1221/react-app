import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Translate } from "react-localize-redux";

import './item.scss';
import { deleteNote } from '../../actions/notes.actions';

class NoteListItem extends Component {
    constructor(props) {
        super(props);

        this.delete = this.delete.bind(this);
    }

    delete() {
        this.props.deleteNote(this.props.id);
    }

  render() {
    return (
      <tr>
          <td>{this.props.id}</td>
          <td><Link to={`/detail/${this.props.id}`}>{this.props.title}</Link></td>
          <td className={`text-right`}>
            <Link to={`/edit/${this.props.id}`}>
                <button className={`btn btn-warning mr-3`}>
                    <Translate id="basic.edit" />
                </button>
            </Link>
            <button onClick={this.delete} className={`btn btn-danger`}>
                <Translate id="basic.delete" />
            </button>
          </td>
      </tr>
    )
  }
}

NoteListItem.propTypes = {
    id: PropTypes.number,
    title: PropTypes.string,
    deleteNote: PropTypes.func,
};

const mapDispatchToProps = (dispatch) => ({ 
    deleteNote: (id) => {
        deleteNote(id)(dispatch);
    }
});

export default connect(null, mapDispatchToProps)(NoteListItem);