import React, { Component } from 'react'
import { editNote, loadNoteDetail } from '../../actions/notes.actions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter, Link } from 'react-router-dom';
import { Translate } from 'react-localize-redux';

import './index.scss';

class NoteEdit extends Component {

    constructor(props) {
        super(props);

        const { noteid } = props.match.params;

        this.noteId = noteid;
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);

        this.state = {
            innerText: this.props.noteDetail ? this.props.noteDetail : '',
        };
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.editNote(this.noteId, this.state.innerText);
        this.props.history.push("/");
    }

    handleChange(event) {
        this.setState({ innerText: event.target.value });
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div className={`row`}>
                    <div className={`col-12`}>
                        <Translate id="basic.title" />:
                        <br />
                        <textarea className={`textarea-input`} type="text" value={this.state.innerText} onChange={this.handleChange} />
                    </div>
                </div>
                <div className={`row mt-3`}>
                    <div className={`col-12 text-right`}>
                        <Link to="/">
                            <button className={`btn btn-warning`} type="button">
                                <Translate id="basic.cancel" />
                            </button>
                        </Link>
                        <button className={`btn btn-success ml-3`} type="submit">
                            <Translate id="basic.edit" />
                        </button>
                    </div>
                </div>
            </form>
        )
    }

    componentDidMount() {
        this.props.loadNoteDetail(this.noteId);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.noteDetail !== this.props.noteDetail) {
            this.setState({ innerText: this.props.noteDetail });
        }
    }
}

NoteEdit.propTypes = {
    editNote: PropTypes.func,
    noteDetail: PropTypes.string,
    loadNoteDetail: PropTypes.func,
    match: PropTypes.object,
    history: PropTypes.object,
};

const mapStateToProps = (state) => ({ noteDetail: state.noteDetail });

const mapDispatchToProps = (dispatch) => ({ 
    editNote: (id, newDetail) => {
        editNote(id, newDetail)(dispatch);
    },
    loadNoteDetail: (id) => {
        loadNoteDetail(id)(dispatch);
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NoteEdit));