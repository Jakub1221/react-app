import React from "react"
import ReactDOM from "react-dom"
import { Provider } from "react-redux"
import { createStore } from "redux";
import { BrowserRouter } from 'react-router-dom'
import { LocalizeProvider } from "react-localize-redux";

import App from "./components/App"
import notesReducer from "./reducers/notes.reducer";

import 'bootstrap/dist/css/bootstrap.css';
import './styles/global.scss';

const store = createStore(notesReducer)

ReactDOM.render(
    <Provider store={store}>
        <LocalizeProvider>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </LocalizeProvider>
    </Provider>,
    document.getElementById("root"),
);