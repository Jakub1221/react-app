import {
    NOTES_LOAD_SUCCESS,
    NOTES_LOAD_FAILED,
    NOTES_DELETE_SUCCESS,
    NOTES_DELETE_FAILED,
    NOTES_DETAIL_SUCCESS,
    NOTES_DETAIL_FAILED,
    NOTES_EDIT_SUCCESS,
    NOTES_EDIT_FAILED,
    NOTES_NEW_SUCCESS,
    NOTES_NEW_FAILED,
} from "./notes.action-types";
import { API_URL } from "../config";
import Axios from "axios";

const apiRequest = (path, type, data, successCallback, failureCallback) => {
    Axios.request({
        method: type,
        url: `${API_URL}${path}`,
        data
    })
    .then(successCallback)
    .catch(failureCallback);
};

export const loadNotes = () => {
    return dispatch => {
        apiRequest(
            'notes',
            'get',
            null,
            (res) => dispatch(loadNotesSuccess(res.data)),
            (err) => dispatch(loadNotesFailed(err))
            );
    };
};

const loadNotesSuccess = (response) => ({
    type: NOTES_LOAD_SUCCESS,
    payload: response,
});

const loadNotesFailed = (err) => ({
    type: NOTES_LOAD_FAILED,
    payload: err,
});

export const deleteNote = (noteId) => {
    return dispatch => {
        apiRequest(
            `notes/${noteId}`,
            'delete',
            null,
            () => dispatch(deleteNoteSuccess(noteId)),
            (err) => dispatch(deleteNoteFailed(err))
            );
    };
};

const deleteNoteSuccess = (noteid) => ({
    type: NOTES_DELETE_SUCCESS,
    payload: noteid,
});

const deleteNoteFailed = (err) => ({
    type: NOTES_DELETE_FAILED,
    payload: err,
});

export const editNote = (noteId, newDetail) => {
    return dispatch => {
        apiRequest(
            `notes/${noteId}`,
            'put',
            { title: newDetail },
            () => dispatch(editNoteSuccess()),
            (err) => dispatch(editNoteFailed(err))
            );
    };
};

const editNoteSuccess = () => ({
    type: NOTES_EDIT_SUCCESS,
    payload: null,
});

const editNoteFailed = (err) => ({
    type: NOTES_EDIT_FAILED,
    payload: err,
});

export const loadNoteDetail = (noteId) => {
    return dispatch => {
        apiRequest(
            `notes/${noteId}`,
            'get',
            null,
            (res) => dispatch(loadNoteDetailSuccess(res.data)),
            (err) => dispatch(loadNoteDetailFailed(err))
            );
    };
};

const loadNoteDetailSuccess = (response) => ({
    type: NOTES_DETAIL_SUCCESS,
    payload: response,
});

const loadNoteDetailFailed = (err) => ({
    type: NOTES_DETAIL_FAILED,
    payload: err,
});

export const newNote = (title) => {
    return dispatch => {
        apiRequest(
            'notes',
            'post',
            {
                title
            },
            (res) => dispatch(newNoteSuccess(res.data)),
            (err) => dispatch(newNoteFailed(err))
            );
    };
};

const newNoteSuccess = (response) => ({
    type: NOTES_NEW_SUCCESS,
    payload: response,
});

const newNoteFailed = (err) => ({
    type: NOTES_NEW_FAILED,
    payload: err,
});