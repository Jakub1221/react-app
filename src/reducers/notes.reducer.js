import {
    NOTES_LOAD_SUCCESS,
    NOTES_LOAD_FAILED,
    NOTES_DETAIL_SUCCESS,
    NOTES_DETAIL_FAILED,
    NOTES_DELETE_SUCCESS,
    NOTES_DELETE_FAILED,
    NOTES_EDIT_SUCCESS,
    NOTES_EDIT_FAILED,
    NOTES_NEW_SUCCESS,
    NOTES_NEW_FAILED,
} from "../actions/notes.action-types";

const initState = {
    firstLoad: true,
    notes: [],
    noteDetail: '',
};

function reducer(state = initState, action) {
    switch (action.type) {
        case NOTES_LOAD_SUCCESS:
            return {
                ...state,
                firstLoad: false,
                notes: [...action.payload],
            };

        case NOTES_LOAD_FAILED:
            return state;

        case NOTES_DETAIL_SUCCESS:
            return {
                ...state,
                noteDetail: action.payload.title,
                noteDetailWaiting: false,
            };

        case NOTES_DETAIL_FAILED:
            return {
                ...state,
                noteDetailWaiting: false,
            };

        case NOTES_DELETE_SUCCESS:
            return {
                ...state,
                notes: state.notes.filter(note => note.id !== action.payload)
            };
        case NOTES_DELETE_FAILED:
            return state;

        case NOTES_EDIT_SUCCESS:
            return state;

        case NOTES_EDIT_FAILED:
            return state;

        case NOTES_NEW_SUCCESS:
            return {
                ...state,
                notes: [...state.notes, action.payload]
            };

        case NOTES_NEW_FAILED:
            return state;

        default:
            return state;
    }
}

export default reducer;